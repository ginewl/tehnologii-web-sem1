import axios from 'axios'
import fbemitter from 'fbemitter'
const SERVER = 'https://gineaualaweb-gineewl.c9users.io'
class TournamentStore{
    constructor(e){
    this.content = []
    this.e = e
    this.selected = null
    }
    getAll(){
        axios(SERVER + '/tournaments')
      .then((response) => {
        this.content = response.data
        this.e.emit('TOURNAMENT_LOAD')
      })
      .catch((error) => console.warn(error))
    }
    createOne(tournament){
    axios.post(SERVER + '/tournaments', tournament)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/tournaments/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, tournament){
    axios.put(SERVER + '/tournaments/' + id, tournament)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  getOne(id){
    axios(SERVER + '/tournaments/' + id)
      .then((response) => {
        this.selected = response.data
        this.e.emit('SINGLE_tournament_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  getUsersFromTournament(id){
      axios(SERVER + '/tournaments/' + id + '/users')
      .then((response) =>{
          this.content = response.data
          this.e.emit('TOURNAMENT_USERS_LOAD')
      })
      .catch((error)=>console.warn(error))
  }
}

export default TournamentStore