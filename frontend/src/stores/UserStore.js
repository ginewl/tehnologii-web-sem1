import axios from 'axios'
import fbemitter from 'fbemitter'
const SERVER = 'https://gineaualaweb-gineewl.c9users.io'
class UserStore{
    constructor(e){
    this.content = []
    this.e = e
    this.selected = null
    }
    getAll(){
        axios(SERVER + '/users')
      .then((response) => {
        this.content = response.data
        this.e.emit('USER_LOAD')
      })
      .catch((error) => console.warn(error))
    }
    createOne(user){
    axios.post(SERVER + '/users', user)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/users/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, user){
    axios.put(SERVER + '/users/' + id, user)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  getOne(id){
    axios(SERVER + '/users/' + id)
      .then((response) => {
        this.selected = response.data
        this.e.emit('SINGLE_USER_LOAD')
      })
      .catch((error) => console.warn(error))
  }
}

export default UserStore