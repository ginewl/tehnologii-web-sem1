import React, {Component} from 'react'

class User extends Component{
  constructor(props){
    super(props)
    this.state = {
      username : '',
      password : '',
      name: '',
      email: '',
      birthday: '',
      isEditing : false
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  componentDidMount(){
    this.setState({
      username : this.props.user.username,
      password : this.props.user.password,
      name : this.props.user.name,
      email: this.props.user.email,
      birthday: this.props.birthday
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      isEditing : false,
      username : nextProps.user.username,
      password : nextProps.user.password,
      name : nextProps.user.name,
      email: nextProps.user.email,
      birthday: nextProps.birthday
    })
  }
  render(){
    if (!this.state.isEditing){
      return (
        <div>
          {this.props.user.username} can be contacted at {this.props.user.email}
          <input type="button" value="Edit" onClick={() => this.setState({isEditing : true})} />
          <input type="button" value="Delete" onClick={() => this.props.onDelete(this.props.user.id)} />
          <input type="button" value="Select" onClick={() => this.props.onSelect(this.props.user.id)} />
        </div>  
      )      
    }
    else{
      return (
        <div>
          <input type="password" name="password" onChange={this.handleChange} value={this.state.password} />
          <input type="text" name="name" onChange={this.handleChange} value={this.state.name} />
          <input type="text" name="email" onChange={this.handleChange} value={this.state.email} />
          <input type="date" name="birthday" onChange={this.handleChange} value={this.state.birthday} />
          <input type="button" value="Cancel" onClick={() => this.setState({isEditing : false})} />
          <input type="button" value="Save" onClick={() => this.props.onSave(this.props.user.id, {username : this.state.username, password: this.state.password, name: this.state.name, email : this.state.email, birthday: this.state.birthday})} />
        </div>  
      )
    }
  }
}

export default User