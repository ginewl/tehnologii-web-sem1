import React, { Component } from 'react';
import './App.css';
import UserStore from '../stores/UserStore'
import User from './User'
import UserForm from './UserForm'
import TournamentStore from '../stores/TournamentStore'
import Tournament from './Tournament'
import TournamentForm from './TournamentForm'
import {EventEmitter} from 'fbemitter'

const e = new EventEmitter()
const store = new UserStore(e)
const tournament_store = new TournamentStore(e)

function addUser(user){
  store.createOne(user)
}

function deleteUser(id){
  store.deleteOne(id)
}

function saveUser(id, user){
  store.saveOne(id, user)
}

function addTournament(tournament){
  tournament_store.createOne(tournament)
}

function deleteTournament(id){
  tournament_store.deleteOne(id)
}

function saveTournament(id, tournament){
  tournament_store.saveOne(id, tournament)
}

function getAllFromTournament(id){
  tournament_store.getUsersFromTournament(id)
}

class App extends Component {
  
  constructor(props){
    super(props)
    this.state = {
      users : [],
      tournaments: [],
      usersFromTournaments: [],
      detailsFor : 0,
      selected : null
    }
    this.selectUser = (id) => {
      store.getOne(id)
      e.addListener('SINGLE_USER_LOAD', () => {
        this.setState({
          selected : store.selected,
          detailsFor : store.selected.id
        })
      })
      e.addListener('SINGLE_tournament_LOAD', () =>{
        this.setState({
          selected: tournament_store.selected,
          detailsFor: tournament_store.selected.id
        })
      })
    }
  }
   componentDidMount(){
    store.getAll()
    e.addListener('USER_LOAD', () => {
      this.setState({
        users : store.content
      })
    })
    e.addListener('TOURNAMENT_LOAD', () =>{
      this.setState({
        tournaments:tournament_store.content
      })
    })
    e.addListener('TOURNAMENT_USERS_LOAD', ()=>{
      this.setState({
        usersFromTournaments:tournament_store.content
      })
    })
  }
  render() {
    if (!this.state.detailsFor){
      return (
        <div class>
          <div>
            <h3>User list</h3>
            {
              this.state.users.map((u) => 
                <User user={u} key={u.id} onDelete={deleteUser} onSave={saveUser} onSelect={this.selectUser} />
              )
            }
          </div>
          <div>
            <h3>Register another user</h3>
            <UserForm onAdd={addUser} />
          </div>
          <div>
          <h3>Tournament list</h3>
            {
              this.state.tournaments.map((t) => 
                <Tournament tournament={t} key={t.id} onDelete={deleteTournament} onSave={saveTournament} onSelect={this.selectTournament} onSelectUsers={getAllFromTournament}/>
                  )}
               {
               this.state.usersFromTournaments.map((u) => 
                <User user={u} key={u.id} onDelete={deleteUser} onSave={saveUser} onSelect={this.selectUser} />
              )
            }
          </div>
          <div>
            <h3>Create another Tournament</h3>
            <TournamentForm onAdd={addTournament} />
          </div>
        </div>
      )
    }
    else{
      return (
        <div>
          Details for: {this.state.selected.name}
          {this.state.selected.username}
          {this.state.selected.password}
          {this.state.selected.email}
          {this.state.selected.birthday}
          <input type="button" value="back" onClick={() => this.setState({detailsFor : 0})} />
        </div>
      )
    }
  }
}

export default App;
