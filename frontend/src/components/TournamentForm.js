import React, {Component} from 'react'

class TournamentForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      title : '',
      description : '',
      number_of_users: '',
      igdb_game_id: '',
      timestamp:''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render(){
    return (
      <div>
        Title : <input type="text" name="title" onChange={this.handleChange}/>
        Description: <input type="text" name="description" onChange={this.handleChange} />
        Number of Users: <input type="number" name="number_of_users" onChange={this.handleChange} />
        IGDB Game ID: <input type="number" name="igdb_game_id" onChange={this.handleChange} />
        Event Date: <input type="date" name="timestamp" onChange={this.handleChange} />
        <input type="button" value="add" onClick={() => this.props.onAdd({title : this.state.title, description : this.state.description, number_of_users: this.state.number_of_users, igdb_game_id: this.state.igdb_game_id, timestamp: this.state.timestamp})} />
      </div>  
    )
  }
}

export default TournamentForm