import React, { Component } from 'react';
import './App.css';
import axios from 'axios'

class Tournament extends Component {
  
  constructor(props){
    super(props)
    this.state = {
      title : '',
      description : '',
      number_of_users: '',
      igdb_game_id: '',
      timestamp:'',
      isEditing : false
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
    
  }
  componentDidMount(){
     /* axios.get("https://api-2445582011268.apicast.io/games/27081?fields=*", {headers:{'Accept': 'application/json', 'user-key':'469fe1b0831b8e207c7a92b045d3f1c8'}})
      .then(res => {
        const data = res.name
        this.setState({ data });
      }).catch( (error) => {
        console.log(error);
      }); */
      this.setState({
      title : this.props.tournament.title,
      description : this.props.tournament.description,
      number_of_users: this.props.tournament.number_of_users,
      igdb_game_id: this.props.tournament.igdb_game_id,
      timestamp: this.props.tournament.timestamp
    })
  }
    componentWillReceiveProps(nextProps){
    this.setState({
      isEditing : false,
      title : nextProps.tournament.title,
      description : nextProps.tournament.description,
      number_of_users: nextProps.tournament.number_of_users,
      igdb_game_id: nextProps.tournament.igdb_game_id,
      timestamp: nextProps.tournament.timestamp
    })
  }
  render() {
   if (!this.state.isEditing){
      return (
        <div>
          {this.props.tournament.title} has a capacity of {this.props.tournament.number_of_users}
          <input type="button" value="edit" onClick={() => this.setState({isEditing : true})} />
          <input type="button" value="delete" onClick={() => this.props.onDelete(this.props.tournament.id)} />
          <input type="button" value="select" onClick={() => this.props.onSelect(this.props.tournament.id)} />
          <input type="button" value="selectUsers" onClick={() => this.props.onSelectUsers(this.props.tournament.id)} />
        </div>  
      )      
    }
    else{
      return (
        <div>
          <input type="text" name="title" onChange={this.handleChange} value={this.state.title} />
          <input type="text" name="description" onChange={this.handleChange} value={this.state.description} />
          <input type="number" name="number_of_users" onChange={this.handleChange} value={this.state.number_of_users} />
          <input type="number" name="igdb_game_id" onChange={this.handleChange} value={this.state.igdb_game_id} />
          <input type="date" name="timestamp" onChange={this.handleChange} value={this.state.timestamp} />
          <input type="button" value="cancel" onClick={() => this.setState({isEditing : false})} />
          <input type="button" value="save" onClick={() => this.props.onSave(this.props.tournament.id, {title : this.state.title, description : this.state.description, number_of_users : this.state.number_of_users, igdb_game_id : this.state.igdb_game_id, timestamp : this.state.timestamp})} />
        </div>  
      )
    }
  }
}

export default Tournament;
