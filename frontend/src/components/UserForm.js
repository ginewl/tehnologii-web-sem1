import React, {Component} from 'react'

class UserForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      username : '',
      password : '',
      name: '',
      email: '',
      birthday: ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render(){
    return (
      <div>
        Username : <input type="text" name="username" onChange={this.handleChange}/>
        Password: <input type="password" name="password" onChange={this.handleChange} />
        Name: <input type="text" name="name" onChange={this.handleChange} />
        Email: <input type="text" name="email" onChange={this.handleChange} />
        Birthday: <input type="date" name="birthday" onChange={this.handleChange} />
        <input type="button" value="add" onClick={() => this.props.onAdd({username: this.state.username, password: this.state.password, name : this.state.name, email : this.state.email, birthday: this.state.birthday})} />
      </div>  
    )
  }
}

export default UserForm