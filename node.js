const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const sequelize = new Sequelize('rest_messages', 'root', '', {
  dialect : 'mysql',
  define : {
    timestamps : false
  }
})

const User = sequelize.define('user', {
  username : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,20]
    }
  },
  password : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,20]
    }
  },
  name : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,20]
    }
  },
  birthday : {
    type : Sequelize.DATE,
    allowNull : false
  },
  email : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      isEmail :true
    }
  }
}, {
  underscored : true
})

const Tournament = sequelize.define('tournament', {
  title  : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,20]
    }
  },
  description : {
    type : Sequelize.TEXT,
    allowNull : false,
    validate : {
      len : [10,1000]
    }
  },
  number_of_users : {
    type : Sequelize.INTEGER,
    allowNull : false,
  },
  igdb_game_id : {
    type : Sequelize.INTEGER,
    allowNull : false,
  },
  timestamp : {
    type : Sequelize.DATE,
    allowNull : false,
  }
})

Tournament.hasMany(User)

const app = express()
app.use(bodyParser.json())

app.get('/create', (req, res, next) => {
  sequelize.sync({force : true})
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

app.get('/users' , (req, res, next) => {
  User.findAll()
    .then((users) => res.status(200).json(users))
    .catch((error) => next(error))
})

app.post('/users' , (req, res, next) => {
  User.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

app.get('/users/:id' , (req, res, next) => {
  User.findById(req.params.id)
    .then((user) => {
      if (user){
        res.status(200).json(user)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))
})

app.put('/users/:id' , (req, res, next) => {
  User.findById(req.params.id)
    .then((user) => {
      if (user){
        return user.update(req.body, {fields : ['username', 'password', 'name', 'birthday', 'email']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

app.delete('/users/:id' , (req, res, next) => {
    User.findById(req.params.id)
    .then((user) => {
      if (user){
        return user.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

app.get('/tournaments' , (req, res, next) => {
  Tournament.findAll()
    .then((tournaments) => res.status(200).json(tournaments))
    .catch((error) => next(error))
})

app.post('/tournaments' , (req, res, next) => {
  Tournament.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

app.get('/tournaments/:id' , (req, res, next) => {
  Tournament.findById(req.params.id)
    .then((tournament) => {
      if (tournament){
        res.status(200).json(tournament)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))
})

app.put('/tournaments/:id' , (req, res, next) => {
  Tournament.findById(req.params.id)
    .then((tournament) => {
      if (tournament){
        return tournament.update(req.body, {fields : ['title', 'description', 'number_of_users', 'igdb_game_id', 'timestamp']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

app.delete('/tournaments/:id' , (req, res, next) => {
    Tournament.findById(req.params.id)
    .then((tournament) => {
      if (tournament){
        return tournament.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

app.get('/tournaments/:tid/users' , (req, res, next) => {
  Tournament.findById(req.params.tid)
    .then((tournament) => {
      if (tournament){
        return tournament.getUsers()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then((users) => {
      if (!res.headersSent){
        res.status(200).json(users)
      }
    })
    .catch((error) => next(error))
})

app.post('/tournaments/:tid/users' , (req, res, next) => {
  Tournament.findById(req.params.tid)
    .then((tournament) => {
      if (tournament){
        let user = req.body
        user.tournament_id = tournament.id
        return Tournament.create(tournament)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('created')
      }
    })
    .catch((error) => next(error))  
})

app.get('/tournaments/:tid/users/:uid' , (req, res, next) => {
  User.findById(req.params.uid, {
      where : {
        tournament_id : req.params.tid
      }
    })
    .then((user) => {
      if (user){
        res.status(200).json(user)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))  
})

app.put('/tournaments/:tid/users/:uid' , (req, res, next) => {
  User.findById(req.params.uid)
    .then((user) => {
      if (user){
        return user.update(req.body, {fields : ['username', 'password', 'name', 'birthday', 'email']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})
app.delete('/tournaments/:tid/users/:uid' , (req, res, next) => {
  User.findById(req.params.uid)
    .then((user) => {
      if (user){
        return user.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((error) => next(error))
})

app.use((err, req, res, next) =>{
  console.warn(err)
  res.status(500).send('some error')
})

app.listen(8080)