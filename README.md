    Aplicația va oferi utilizatorului posibilitatea de a selecta un joc dintr-o lista preluată de pe IGDB în funcție de genul jocului și de a crea un turneu în care urmează să se
 înregistreze jucătorii.
    După ce este selectat id-ul jocului se face un GET pentru a prelua informațiile de pe IGDB, informații ce vor fi afișate și pe pagina cu turneul.
    Vor exista și pagini prestabilite pentru jocuri populare(FIFA, CS:GO, LoL etc.) pe care se vor prelua direct informațiile jocurilor și se vor putea introduce date în scopul 
 organizării turneelor respective.
    Fiecare formular de înregistrare a datelor pentru turneu va conține câmpuri pentru numele turneului, numărul jucătorilor, numele jucătorilor, dar și 
 id-ul jocului(ce va fi preluat din pagina care direcționează utilizatorul către formularul de înregistrare).
    La final se va afișa pagina cu turneul creat, conținând informațiile aferente. 
    Va exista și o pagina care va afișa toate turneele create, turnee ce sunt salvate în baza de date.
 
 
 UPDATE FAZA II 9.12.2017
 
1. se va executa comanda mysql-ctl start pentru a deschide mysql
2. se va executa comanda mysql -u root pentru a defini ruta
3. se va executa comanda create database IGDBTournaments; pentru a crea baza de date
4. în acest moment pornim serverul executând comanda node node.js
5. în acest moment avem baza de date creată, dar și tabelele din aceasta(prin executarea node.js)
6. pentru tabela User, pentru a insera în tabela utilizăm Postman. Folosim metoda POST si copiem url-ul din c9../user. În tab-ul "Body", 
selectăm JSON. Send și, dacă datele au fost introduse corect, s-a inserat in baza de date.
7. folosim metoda GET pentru a testa pasul anterior
8. metoda PUT face update pe baza de date și modifică obiectul cu id-ul dat
9. metoda DELETE va șterge obiectul cu id-ul dat
10. obiectul Tournament folosește un foreign key care comunică cu tabela User
11. metoda get/tournaments/id/users va selecta userii de la turneul cu id-ul dat